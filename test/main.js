
var chai                                    = require('chai'),
    chaiHttp                                = require('chai-http'),
    assert                                  = chai.assert,
    apiSecure                               = require('../api-secure.js'),
    serverMock                              = require("./support/serverMock.js"),
    apiSecureClean                          = new apiSecure(),
    apiSecureRouter                         = new apiSecure({router : serverMock.router});

    chai.use(chaiHttp)

    apiSecureClean.phrase                   = 'Custom Test Phrase';
    apiSecureClean.jwtOpt.cert              = 'Custom Test Cert';

    apiSecureRouter.phrase                  = 'Custom Test Phrase';
    apiSecureRouter.jwtOpt.cert             = 'Custom Test Cert';

    // apiSecureClean.setLogLvl("silly");
    // apiSecureRouter.setLogLvl("silly");

    describe('encrypt && decrypt', function() {
        let phraseToEncrypt                 = 'Custom Test Text',
            knowResultOfEncrypt             = 'BSATAGLXPcH6fV3iYV86jNBIH3AtALT98n5cvIjpzsw=';

        it('Golden Path', function(){
            assert.equal(apiSecureClean.encrypt(phraseToEncrypt), knowResultOfEncrypt, 'Encryption fail with expected value');
            assert.equal(apiSecureClean.decrypt(knowResultOfEncrypt), phraseToEncrypt, 'Decryption fail with expected value');        
        });
    });

    describe('create && decode', function() {
        let customKeyVal                    = 'customVal',
            token                           = apiSecureClean.create({params:{customKey:customKeyVal}}),
            decoded                         = apiSecureClean.decode(token);

        it('Golden Path', function(){
            var callbackResult              = false

            assert.isString(token, 'Should be a String type');
            assert.equal(decoded.customKey, customKeyVal, 'Integrity in phrase fail, should be equal to the encrypted value');        

            assert.deepEqual(apiSecureClean.decode(token), decoded, 'The decoded result should be deepEqual with the expected result')
        });
    });

    describe('setSecurityCheck "https"', function() {
        apiSecureRouter.setSecurityCheck({
            accessPoints                : '/https'
          , checks                      : ['https']
        });

        apiSecureRouter.router.route('/https')
            .get(function(req, res){
                res.status(200).json({})
            });

        apiSecureRouter.router.route('/http')
            .post(function(req, res){
                res.status(200).json({})
            })

        it('Golden Path -> No HTTPS', function(done){
            chai.request("localhost:9988")
                .get('/https')
                .end(function(err, res) {
                    assert.equal(res.status, 401, "Should return status 401 for this protected endpoint")
                    done();
                });
        });

        it('Golden Path -> HTTP', function(done){
            chai.request("localhost:9988")
                .post('/http')
                .end(function(err, res) {
                    assert.equal(res.status, 200, "Should return status 200 for this endpoint")
                    done();
                });
        });
    }); 


    describe('setSecurityCheck "token"', function() {
        let token                       = apiSecureRouter.create(),
            expiredToken                = apiSecureRouter.create({ expireTime : -1 });

        apiSecureRouter.setSecurityCheck({
            accessPoints                : '/token'
          , checks                      : ['token']
        });

        apiSecureRouter.router.route('/token')
            .get(function(req, res){
                res.status(200).json({})
            });

        apiSecureRouter.router.route('/noToken')
            .get(function(req, res){
                res.status(200).json({})
            })

        it('Golden Path -> noToken & protected', function(done){
            chai.request("localhost:9988")
                .get('/token')
                .end(function(err, res) {
                    assert.equal(res.status, 401, "Should return status 401 for this protected endpoint")
                    done();
                });
        });

        it('Golden Path -> noToken & unprotected', function(done){
            chai.request("localhost:9988")
                .get('/noToken')
                .end(function(err, res) {
                    assert.equal(res.status, 200, "Should return status 200 for this unprotected endpoint")
                    done();
                });
        });        

        it('Golden Path -> token & protected', function(done){
            chai.request("localhost:9988")
                .get('/token')
                .set(apiSecureRouter.tokenHeader, token)
                .end(function(err, res) {
                    assert.equal(res.status, 200, "Should return status 200 for this endpoint")
                    done();
                });
        });

        it('Golden Path -> token & expire', function(done){
            chai.request("localhost:9988")
                .get('/token')
                .set(apiSecureRouter.tokenHeader, expiredToken)
                .end(function(err, res) {
                    assert.equal(res.status, 401, "Should return status 401 for this protected endpoint (Because of expire date)")
                    done();
                });
        });
    });


/*
    @todo => Remove enterily from `api-secure` should be only in `api-utils`
*/
// describe('getRandomInt', function() {
//     it('Golden Path', function(){
//         assert.isNumber(apiSecureInstance.getRandomInt(3,5),   'Should be a number');
//         assert.isAbove(apiSecureInstance.getRandomInt(3,5), 2, 'Should be above of 2');
//         assert.isBelow(apiSecureInstance.getRandomInt(3,5), 6, 'Should be below of 6');
//     });
// });

// describe('isRequestDataValid', function(){
//     it('Golden Path', function() {
//         assert.isTrue(apiSecureEmpty.isRequestDataValid(1,1),  'Should be True');
//         assert.isNotTrue(apiSecureEmpty.isRequestDataValid(1,0), 'Should be False');
//     })

//     it('Coercion Path', function() {
//         assert.isTrue(apiSecureEmpty.isRequestDataValid(1,"1"),  'Should be True');
//         assert.isNotTrue(apiSecureEmpty.isRequestDataValid(1,"0"), 'Should be False');
//     })    
// });




    // testSuit                                = function(){};

        // describe('setSecurityCheck', function() {

        //     it('Golden Path -> HTTPS', function(){
        //         apiSecureInstance.setSecurityCheck({
        //             accessPoints                : '/*'
        //           , checks                      : ['https']
        //         });

        //         // var callbackResult          = false

        //         // assert.isString(token, 'Should be a String type');
        //         // assert.equal(decoded.customKey, customKeyVal, 'Integrity in phrase fail, should be equal to the encrypted value');        

        //         // assert.deepEqual(apiSecureInstance.decode(token), decoded, 'The decoded result should be deepEqual with the expected result')
        //     });
        // }); 

// app.listen(9988, '0.0.0.0', function() {
//   console.log("server starting on " + appEnv.url);
//   testSuit();
// });

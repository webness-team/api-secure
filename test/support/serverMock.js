module.exports                              = new serverMock();


function serverMock(){
    var express                                 = require('express'),
        compression                             = require('compression'),
        app                                     = express(),
        router                                  = express.Router();
    
        this.app                                = app;
        this.router                             = router;

        app.use(router);
        app.listen(9988, 'localhost', function() {
            // console.log("server starting on " + 9988);
        });
}

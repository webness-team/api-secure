/*
 * Api-Secure
 *
 * @author   Mobile Dev Team - IBM Argentina <joeldelatorriente@ar.ibm.com>
 */

'use strict'

module.exports                      = apiSecure

/**
 *@module
 *@requires crypto
 *@requires jsonwebtoken
 *@requires cors
 */

var crypto                          = require('crypto'),
    jwt                             = require('jsonwebtoken'),
    cors                            = require('cors'),
    winston                         = require("winston"),
    apiUtils                        = new (require("api-utils"))(),
    defaults                        = {
        phrase                      : false,
        algorithm                   : "aes256",
        plainEncoding               : "utf8",
        encryptEncoding             : "urlBase64",
        jwtAlgorithm                : "HS256",
        expireTime                  : 15,
        cert                        : false,
        tokenHeader                 : "secure",
        responses                   : {
            unauthorized            : function(res){
                // @todo -> Replace with a common response
                res.status(401).json({"success":false, "data": {}, "message":"unauthorized"});
            },
            badProtocol             : function(res){
                // @todo -> Replace with a common response
                res.status(401).json({"success":false, "data": {}, "message":"badProtocol"});
            }
        }
    };

winston.level                       = "error"

/*v1.1.x Draft*/

/**
 * @class
 */
function apiSecure(options){
    var options                     = options                   || {},
        jwtOpt                      = options.jwt               || {};

    this.phrase                     = options.phrase            || defaults.phrase;
    this.algorithm                  = options.algorithm         || defaults.algorithm;
    this.plainEncoding              = options.plainEncoding     || defaults.plainEncoding;
    this.encryptEncoding            = options.encryptEncoding   || defaults.encryptEncoding;
    this.tokenHeader                = options.tokenHeader       || defaults.tokenHeader

    this.jwtOpt                     = {
        phrase                      : jwtOpt.phares             || this.phrase,
        algorithm                   : jwtOpt.algorithm          || defaults.jwtAlgorithm,
        expireTime                  : jwtOpt.expireTime         || defaults.expireTime,
        cert                        : jwtOpt.cert               || defaults.cert
    };

    this.responses                  = options.responses         || defaults.responses;
    this.router                     = options.router            || false;


    for(let response in defaults.responses){
        this.responses[response]    = (this.responses[response])?this.responses[response]:defaults.responses[response];
    }
}

/**
 * Create a `jwt` token with an encryption envelop
 * @param {object} options                  - Object with override options that could had the follow attributes
 * @param {string} options.cert             - The private key / cert to use with `jwt` to sign
 * @param {string} options.expireTime       - The expiration time (Minutes), this time will be added to the server time to set the expire. Default is `15`
 * @param {string} options.phrase           - Phrase / Password used to sign the `jwt` token
 * @param {string} options.algorithm        - Algorithm used by `jwt.sign`
 * @return {string|false} encrypted token with or false if the process fail.
 */
apiSecure.prototype.create          = function(options){
    var options                     = options                   || {},
        cert                        = options.cert              || this.jwtOpt.cert,
        expireTime                  = options.expireTime        || this.jwtOpt.expireTime,
        phrase                      = options.phrase            || this.jwtOpt.phrase   || this.phrase,
        algorithm                   = options.algorithm         || this.jwtOpt.algorithm,
        token                       = false,
        expireDate                  = new Date();

    options.params                  = options.params            || {};
    expireDate.setMinutes(expireDate.getMinutes() + expireTime);

    var tokenArguments              = {
        expire                      : expireDate.getTime(),
        phrase                      : phrase
    }

    for (let key in options.params) {
        tokenArguments[key]         =  options.params[key]
    }

    token                           = jwt.sign(tokenArguments, cert, { algorithm : algorithm});
    token                           = this.encrypt(token);

    var tokenObj                    = {
        token                       : token,
        expire                      : expireDate.getTime(),
    };

    return tokenObj;
}

/**
 * Decode a token and check if is sign by `jwt` and if the inner phrase match.
 * @param {string} token                    - Token.
 * @param {object} options                  - Object with override options that could had the follow attributes
 * @param {string} options.cert             - The private key / cert to use with `jwt` to verify
 * @param {string} options.phrase           - Phrase / Password used to verify the `jwt` token
 * @param {string} options.algorithm        - Algorithm used by `jwt.verify`
 * @param {function} callback               - Callback that is executed if passed, with the the following args, `err` (true if error) and `decoded` (decoded token or undefined if fail)
 */
apiSecure.prototype.decode          = function(token, options, callback){
    var options                     = options                   || {},
        cert                        = options.cert              || this.jwtOpt.cert,
        phrase                      = options.phrase            || this.jwtOpt.phrase   || this.phrase,
        algorithm                   = options.algorithm         || this.jwtOpt.algorithm,
        decrypted                   = this.decrypt(token),
        decoded                     = false;

    try{
        decoded                     = (decrypted)?jwt.verify(decrypted, cert, { algorithms: [algorithm] }):false;
    }
    catch (e){
        winston.log("error", "apiSecure -> decode => 'Error while decoding / verify' - name::"+e.name+" code:: "+e.code+" msg:: "+e.message);
        decoded                     = false;
    }

    if(!decoded || !decoded.hasOwnProperty("phrase") || decoded.phrase !== phrase){
        winston.log("info", "apiSecure -> decode => 'Error phrase check'");
        decoded                     = false;
    }

    if(!decoded || !decoded.hasOwnProperty("expire") || decoded.expire < Date.now()){
        winston.log("info", "apiSecure -> decode => 'Error expireDate check'");
        decoded                     = false;
    }

    if(callback){
        callback(!decoded, decoded);
    }
    else{
        return decoded;
    }
}


/**
 * Text encryption function.
 * @param {string} toEncrypt                - Text to encrypt.
 * @param {object} options                  - Object with override options that could had the follow attributes
 * @param {string} options.phrase           - phrase that will use as cipher password (Override the setted phrase in the instance)
 * @param {string} options.algorithm        - Name of the cipher algorithm to use (Override the default algorithm `aes256`)
 * @param {string} options.inputEncoding    - Name of the encoding of the `toEncrypt` (Override the default value `utf8`)
 * @param {string} options.outputEncoding   - Name of the encoding used for the encrypted result (Override the default value `urlBase64`)
 * @return {string|false}.
 */
apiSecure.prototype.encrypt         = function(toEncrypt, options){
    let result                      = false;
    var options                     = options                   || {},
        phrase                      = options.phrase            || this.phrase,
        algorithm                   = options.algorithm         || this.algorithm,
        inputEncoding               = options.inputEncoding     || this.plainEncoding,
        outputEncoding              = options.outputEncoding    || this.encryptEncoding,
        realOutputEncoding          = ((outputEncoding==="urlBase64")?"base64":outputEncoding);

    if(phrase){
        try{
            var cipher              = crypto.createCipher(algorithm, phrase);
            result                  = cipher.update(toEncrypt, inputEncoding, realOutputEncoding);
            result                  += cipher.final(realOutputEncoding);
        
            if(outputEncoding === "urlBase64"){
                result              = result.replace(/\+/g, "-").replace(/\//g, "_");
            }
        }
        catch(e){
            winston.log("error", "apiSecure -> encrypt => 'Error on encryption' - name::"+e.name+" code:: "+e.code+" msg:: "+e.message);
            result                  = false
        }        
    }
    else{
        winston.log("info", "apiSecure -> encrypt => 'No valid password / phrase'");        
    }

    return result;
}


/**
 * Text decryption function.
 * @param {string} toDecrypt                - Text to decrypt.
 * @param {object} options                  - Object with override options that could had the follow attributes
 * @param {string} options.phrase           - phrase that will use as decipher password (Override the setted phrase in the instance)
 * @param {string} options.algorithm        - Name of the decipher algorithm to use (Override the default algorithm `aes256`)
 * @param {string} options.inputEncoding    - Name of the encoding of the `toDecrypt` (Override the default value `urlBase64`)
 * @param {string} options.outputEncoding   - Name of the encoding used for the decrypted result (Override the default value `utf8`)
 * @return {string|false}
 */
apiSecure.prototype.decrypt         = function(toDecrypt, options){
    let result                      = false;
    var options                     = options                   || {},
        phrase                      = options.phrase            || this.phrase,
        algorithm                   = options.algorithm         || this.algorithm,
        inputEncoding               = options.inputEncoding     || this.encryptEncoding,
        outputEncoding              = options.outputEncoding    || this.plainEncoding,
        realInputEncoding           = ((inputEncoding==="urlBase64")?"base64":inputEncoding);

    if(toDecrypt && phrase){
        try{
            if(inputEncoding === "urlBase64"){
                toDecrypt           = toDecrypt.replace(/-/g, "+").replace(/_/g, "/");
            }

            var decipher            = crypto.createDecipher(algorithm, phrase);
            result                  = decipher.update(toDecrypt, realInputEncoding, outputEncoding);
            result                  += decipher.final(outputEncoding);
        }
        catch(e){
            winston.log("error", "apiSecure -> decrypt => 'Error on decryption' - name::"+e.name+" code:: "+e.code+" msg:: "+e.message);        
            result                  = false
        }
    }
    else{
        let msg                     = (toDecrypt)?"apiSecure -> decrypt => 'No valid password / phrase'":"apiSecure -> decrypt => 'No valid token found to decrypt'"; 
        winston.log("info", msg);
    }

    return result;
}

/**
 * Set the security check options.
 *
 * @param {Object} options - Description
 * @todo refactor: eliminate double forEach
*/
apiSecure.prototype.setSecurityCheck = function(options){
    var self                        = this,
        opts                        = {},
        dict                        = {
            'https'                 : this.useHttpsOnly,
            'token'                 : this.useTokenCheck,
            'extraCheck'            : this.useExtraCheck
        };

    options.accessPoints            = apiUtils.ensureArray(options.accessPoints);
    options.checks                  = apiUtils.ensureArray(options.checks);

    options.accessPoints.forEach(function(accessPointItem){
        if(apiUtils.getType(accessPointItem)==='string'){
            accessPointItem         = { path : accessPointItem }
        }

        options.checks.forEach(function(checkItem){
            if(apiUtils.getType(checkItem)==='string'){
                checkItem           = { type : checkItem }
            }
            opts                    = (accessPointItem.options)?accessPointItem.options:opts;

            dict[checkItem.type].call(self, accessPointItem, opts);
        });
    });
};

/**
 * Prevents a given accessPoint of processing any request that does not use HTTPS protocol.
 * It also sets up a default error response to be sent to the client when needed.
 *
 * @param {Object} accessPoint - accessPoint object, that contains at least the path attribute
*/
apiSecure.prototype.useHttpsOnly    = function(accessPoint){
    var self                        = this,
        path                        = accessPoint.path,
        method                      = accessPoint.method || "all";

    self.router[method](path, function(req, res, next){
        if(req.secure && req.protocol==='https'){
            next();
        }
        else{
            self.responses.badProtocol(res);
        }
    })
};

apiSecure.prototype.useTokenCheck   = function(accessPoint, options){
    var self                        = this,
        options                     = options || {},
        exceptions                  = (options.exceptions)?options.exceptions:[],
        path                        = accessPoint.path,
        method                      = accessPoint.method || "all";

    this.router[method](path, function(req, res, next){
        var token                   = self.getClientToken(req),
            isNotException          = true,
            onValidToken            = function(error, tokenInfo){
                if(!error){
                    tokenInfo.token = token;
                    req.tokenInfo   = tokenInfo;
                    var onExtraValidation   = function(success){
                            if(success){
                                next();
                            }
                            else{
                                winston.log("info", "apiSecure -> useTokenCheck -> onValidToken -> onExtraValidation => 'No valid token - extra validation fail'");
                                self.responses.unauthorized(res);
                            }
                        },
                        extraValidation     = options.callback || ((ctx, req, res, tokenInfo, onExtraValidation)=>{onExtraValidation(true)});

                    extraValidation(self, req, res, tokenInfo, onExtraValidation);
                }
                else{
                    winston.log("info", "apiSecure -> useTokenCheck -> onValidToken=> 'No valid token - decode fail'");
                    self.responses.unauthorized(res);
                }
            };

        exceptions.some((itm)=>{
            var result = false
            if(req.path.indexOf(itm.path)!=-1 && req.method.indexOf(itm.method)!=-1){
                isNotException = false;
                result = true;
            }
            return result;
        })

        if(isNotException){
            self.decode(token, {}, onValidToken);
        }
        else{
            next();
        }
    });
};

apiSecure.prototype.useExtraCheck   = function(accessPoint, options){
    var options                     = options || {},
        exceptions                  = (options.exceptions)?options.exceptions:[],
        path                        = accessPoint.path,
        method                      = accessPoint.method || "all";

    this.router[method](path, function(req, res, next){
        var extraCheck              = options.callback || function(){ 
                next(); 
            },
            isNotException          = true;

        exceptions.some((itm)=>{
            if(req.path.indexOf(itm)!=-1){
                isNotException      = false;
                return true;
            }
            return false;
        })

        if(isNotException){
            extraCheck(req, res, next);
        }
        else{
            next();
        }
    });
}


/**
 * Get the client token from the request header, using as header nane the `this.tokenHeader` that could be set in the instance, by default `tokenHeader` is `"secure"`
 * @param {object} req         - request object
 * @return {string|boolean}    - The client token or false if no token is get.
 */
apiSecure.prototype.getClientToken  = function(req){
    var clientToken                 = req.get(this.tokenHeader),
        response                    = (clientToken)?clientToken:false;
    return response;
};

/**
 * Enable CORS
 *
 * @todo enhace: Allow to recevie cors options (as exposedHeaders)
*/
apiSecure.prototype.enableCors      = function(options){
    var self                        = this,
        routes                      = options.routes || [];

    routes.forEach(function(itm){
        self.router.route(itm.route)
            .all(cors(itm.options));
    })
};

/**
    Set the log level (Winston) in this module
    @param {integer} lvl
 */
apiSecure.prototype.setLogLvl       = function(lvl){
    var logLvls                     = ["error", "warn", "info", "verbose", "debug", "silly"],
        lvl                         = isNaN(lvl)?lvl:logLvls[lvl];

    winston.level                   = lvl;
}


/* api-utils wrapper, should be removed in the future, added to grant backward compatibility*/
apiSecure.prototype.getRandomInt    = function(min, max){
    winston.log("info", "apiSecure -> getRandomInt => Deprecation note: This function is deprecated, will be removed in future versions. Should use from 'api-secure' library");    
    return apiUtils.getRandomInt(min, max)
}

/* api-utils wrapper, should be removed in the future, added to grant backward compatibility*/
apiSecure.prototype.randomValueBase64 = function(len){
    winston.log("info", "apiSecure -> randomValueBase64 => Deprecation note: This function is deprecated, will be removed in future versions. Should use from 'api-secure' library");    
    return apiUtils.randomValueBase64(len)
}



